# 半中文编程指南

#### 介绍
现有语言中使用中文编程的代码指南

#### 原因
现在的编程中，中文编程一直是一个争议较大的问题，本人日常自己编程中，使用较多的就是半中文编程了，日常使用中，java,golang,js都基本是半中文编程 
特此，本人就罗列一些自己日常用半中文编程的一些案例， 
本人立场，半中文编程能够有效的降低英文编程的阅读门槛，让代码的可读性更高 


以下简单示例
```
package server

import (
	"fmt"
	"os"
)

// 结构体部分可以使用中文，一般以S开头，struct
type S文件 struct {
	V路径 string //内容一般以V开头
	V内容 []byte
}

// 函数方法一定要大写的英文，此处函数一般用大写的F开头
func F新建文件(路径 string) (文件 *S文件, err error) {
	文件 = &S文件{
		V路径: 路径,
	}
	文件.V内容, err = os.ReadFile(路径)
	if err != nil {
		return
	}

	return
}

func (a *S文件) F尝试打印结果() {
	fmt.Println(string(a.V内容))
}
```
jsu部分
```
let token = "123456789"
let 长度=5
let 基础数据 = "longfei"
let 计算次数 = 0
let 请求 = token.substring(0, 长度)
while (true) {
    let v时间戳 = new Date().getTime();
    let v结果 = 基础数据 + 计算次数
    let md5数据 = hex_md5(v结果 + v时间戳)
    if (请求 == md5数据.substring(0, 长度)) {
        //console.log(v结果 + v时间戳, "结束时间", new Date().getTime() - v时间戳, md5数据)
        self.postMessage({ v结果, v时间戳 });
        return
    }
    计算次数++
}
```