package server

import (
	"fmt"
	"os"
)

// 结构体部分可以使用中文，一般以S开头，struct
type S文件 struct {
	V路径 string //内容一般以V开头
	V内容 []byte
}

// 函数方法一定要大写的英文，此处函数一般用大写的F开头
func F新建文件(路径 string) (文件 *S文件, err error) {
	文件 = &S文件{
		V路径: 路径,
	}
	文件.V内容, err = os.ReadFile(路径)
	if err != nil {
		return
	}

	return
}

func (a *S文件) F尝试打印结果() {
	fmt.Println(string(a.V内容))
}
